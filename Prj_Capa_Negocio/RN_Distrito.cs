﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prj_Capa_Datos;
using System.Data;
namespace Prj_Capa_Negocio
{
    public class RN_Distrito
    {
        public void RN_Registrar_Distrito(string nomDistrito)
        {
            BD_Distrito obj = new BD_Distrito();
            obj.BD_Registrar_Distrito(nomDistrito);

        }
        //editar
        public void RN_Editar_Distrito(int idDis, string nomDistrito)
        {
            BD_Distrito obj = new BD_Distrito();
            obj.BD_Editar_Distrito(idDis,  nomDistrito);
        }
        ///////Mostrar/////////
        public DataTable RN_mostrar_todas_los_Distritos()
        {
            BD_Distrito obj = new BD_Distrito();
            return obj.BD_mostrar_todas_los_Distritos();
        }
        //eliminar marca
        public void RN_Eliminar_Distrito(int idDis)
        {
            BD_Distrito obj = new BD_Distrito();
            obj.BD_Eliminar_Distrito(idDis);
        }
    }
}
