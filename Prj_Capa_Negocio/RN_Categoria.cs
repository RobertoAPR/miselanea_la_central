﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Prj_Capa_Datos;
namespace Prj_Capa_Negocio
{
   public class RN_Categoria
    {
        //Registrar
        public void RN_Registrar_Categoria(string nomCateg)
        {
            BD_Categoria obj = new BD_Categoria();
            obj.BD_Registrar_Categoria(nomCateg);

        }
        //editar
        public void RN_Editar_Categoria(int idcateg, string nomCateg)
        {
            BD_Categoria obj = new BD_Categoria();
            obj.BD_Editar_Categoria(idcateg,nomCateg);
        }
         ///////Mostrar/////////
        public DataTable RN_mostrar_todas_las_categorias()
        {
            BD_Categoria obj = new BD_Categoria();
            return obj.BD_mostrar_todas_las_categorias();
        }
    }
}
