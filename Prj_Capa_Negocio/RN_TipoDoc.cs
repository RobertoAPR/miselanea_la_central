﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Prj_Capa_Datos;
using Prj_Capa_Entidad;

namespace Prj_Capa_Negocio
{
    public class RN_TipoDoc
    {
        public static string RN_NroID(int idtipo)
        {
            return BD_Tipo_Doc.BD_NroID(idtipo);
        }

        public static void RN_Actualizar_SiguienteNro_Correlativo(int idtipo)
        {
             BD_Tipo_Doc.BD_Actualizar_SiguienteNro_Correlativo(idtipo) ;
        }
    }
}
