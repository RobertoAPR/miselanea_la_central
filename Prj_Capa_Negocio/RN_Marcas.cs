﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Prj_Capa_Datos;
using System.Data.SqlClient;

namespace Prj_Capa_Negocio
{
   public class RN_Marca
    {
        //Registrar
        public void RN_Registrar_Marca(string nomMarca)
        {
            BD_Marcas obj = new BD_Marcas();
            obj.BD_Registrar_Marca(nomMarca);

        }
        //editar
        public void RN_Editar_Marca(int idmar, string nomMarca)
        {
            BD_Marcas obj = new BD_Marcas();
            obj.BD_Editar_Marca(idmar, nomMarca);
        }
        ///////Mostrar/////////
        public DataTable RN_mostrar_todas_las_Marcas()
        {
            BD_Marcas obj = new BD_Marcas();
            return obj.BD_mostrar_todas_las_Marcas();
        }
        //eliminar marca
        public void RN_Eliminar_Marca(int idmar)
        {
            BD_Marcas obj = new BD_Marcas();
            obj.BD_Eliminar_Marca(idmar);
        }
    }
}
