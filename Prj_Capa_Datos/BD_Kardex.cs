﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Prj_Capa_Entidad;
using System.Windows.Forms;
using Prj_Capa_Enridad;

namespace Prj_Capa_Datos
{
    public class BD_Kardex: BDConexion
    {
        public static bool seguardo = false;
        public static bool detsaved = false;
        public void BD_Registrar_Kardex(string idkardex,string idproveedor,string idproducto)
        {
            SqlConnection cn = new SqlConnection();

            try
            {
                cn.ConnectionString = Conectar();
                SqlCommand cmd = new SqlCommand("sp_crear_kardex", cn);
                cmd.CommandTimeout = 20;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idkardex", idkardex);
                cmd.Parameters.AddWithValue("@idprod", idproveedor);
                cmd.Parameters.AddWithValue("@idprovee", idproducto);
                

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                seguardo = true;
            }
            catch (Exception ex)
            {
                seguardo = false;
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }

                MessageBox.Show("Error al guardara: " + ex.Message, "Capa Datos Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        // detalle 
        public void BD_Registrar_Detalle_Kardex(EN_Kardex kr)
        {
            SqlConnection cn = new SqlConnection();

            try
            {
                cn.ConnectionString = Conectar();
                SqlCommand cmd = new SqlCommand("Sp_registrar_detalle_kardex", cn);
                cmd.CommandTimeout = 20;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Krdx", kr.Idkardex);
                cmd.Parameters.AddWithValue("@Item", kr.Item);
                cmd.Parameters.AddWithValue("@Doc_Soport", kr.Doc_soporte);
                cmd.Parameters.AddWithValue("@Det_Operacion", kr.Det_Operacion);
                //entrada
                cmd.Parameters.AddWithValue("@Cantidad_In", kr.Cantidad_in);
                cmd.Parameters.AddWithValue("@Precio_Unt_In", kr.Precio_In);
                cmd.Parameters.AddWithValue("@Costo_Total_In", kr.Total_In);
                //salida
                cmd.Parameters.AddWithValue("@Cantidad_Out", kr.Cantidad_Out);
                cmd.Parameters.AddWithValue("@Precio_Unt_Out", kr.Precio_out);
                cmd.Parameters.AddWithValue("@Importe_Total_Out", kr.Total_out);
                //saldo
                cmd.Parameters.AddWithValue("@Cantidad_Saldo", kr.Cantidad_saldo);
                cmd.Parameters.AddWithValue("@Promedio", kr.Promedio);
                cmd.Parameters.AddWithValue("@Costo_Total_Saldo", kr.Total_saldo);
                

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                detsaved = true;
            }
            catch (Exception ex)
            {
                detsaved = false;
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }

                MessageBox.Show("Error al guardara: " + ex.Message, "Capa Datos Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //validar
        public bool BD_Verificar_Producto_siTieneKardex(string idprod)
        {
            bool respuesta = false;
            Int32 getvalue = 0;

            SqlConnection cn = new SqlConnection();
            try
            {
               
                SqlCommand cmd = new SqlCommand();

                cn.ConnectionString = Conectar();

                cmd.CommandText = "Sp_Ver_sihay_Kardex";
                cmd.Connection = cn;
                cmd.CommandTimeout = 20;
                cmd.CommandType = CommandType.StoredProcedure;
                //parametros
                cmd.Parameters.AddWithValue("@Id_Prod", idprod);

                cn.Open();
                getvalue = Convert.ToInt32(cmd.ExecuteScalar());

                if (getvalue > 0)
                {
                    respuesta = true;
                }
                else
                {
                    respuesta = false;
                }

                cmd.Parameters.Clear();
                cmd.Dispose();
                cmd = null;
                cn.Close();
            }catch(Exception ex)
            {
                detsaved = false;
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }

                MessageBox.Show("Error al guardara: " + ex.Message, "Capa Datos Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                respuesta= false;
            }
            return respuesta;
            
        }

    }
}
