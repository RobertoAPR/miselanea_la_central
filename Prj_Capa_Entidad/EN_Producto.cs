﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prj_Capa_Enridad
{
    public class EN_Producto
    {
        private string _idprod;
        private string _idproveedor;
        private string _descripcionGeneral;
        private double _frank;
        private double _PreCompra_Sol;
        private double _PreCompra_Dlr;
        private double _stock;
        private int _idcat;
        private int _idmark;
        private string _foto;
        private double _Preventa_Mnr;
        private double _Preventa_Myr;
        private double _Preventa_Dolr;
        private string _UndMedida;
        private double _PesoUnit;
        private double _UtilidadUnit;
        private string _TipoProducto;
        private double _valorGeneral;

        public double ValorGeneral { get => _valorGeneral; set => _valorGeneral = value; }
        public string TipoProducto { get => _TipoProducto; set => _TipoProducto = value; }
        public double UtilidadUnit { get => _UtilidadUnit; set => _UtilidadUnit = value; }
        public double PesoUnit { get => _PesoUnit; set => _PesoUnit = value; }
        public string UndMedida { get => _UndMedida; set => _UndMedida = value; }
        public double Preventa_Dolr { get => _Preventa_Dolr; set => _Preventa_Dolr = value; }
        public double Preventa_Myr { get => _Preventa_Myr; set => _Preventa_Myr = value; }
        public double Preventa_Mnr { get => _Preventa_Mnr; set => _Preventa_Mnr = value; }
        public string Foto { get => _foto; set => _foto = value; }
        public int Idmark { get => _idmark; set => _idmark = value; }
        public int Idcat { get => _idcat; set => _idcat = value; }
        public double Stock { get => _stock; set => _stock = value; }
        public double PreCompra_Dlr { get => _PreCompra_Dlr; set => _PreCompra_Dlr = value; }
        public double PreCompra_Sol { get => _PreCompra_Sol; set => _PreCompra_Sol = value; }
        public double Frank { get => _frank; set => _frank = value; }
        public string DescripcionGeneral { get => _descripcionGeneral; set => _descripcionGeneral = value; }
        public string Idproveedor { get => _idproveedor; set => _idproveedor = value; }
        public string Idprod { get => _idprod; set => _idprod = value; }
    }
}
