﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Prj_Capa_Datos;
using Prj_Capa_Negocio;

namespace Microsell_Lite.Utilitarios
{
    public partial class Frm_categoria : Form
    {
        public Frm_categoria()
        {
            InitializeComponent();
        }

        private void Frm_Reg_Prod_Load(object sender, EventArgs e)
        {
            configuracion_listView();
            Cargar_Todas_Categorias();
        }

        private void pnl_titu_MouseMove(object sender, MouseEventArgs e)
        {
            Utilitario obj = new Utilitario();

            if (e.Button == MouseButtons.Left)
            {
                obj.Mover_formulario(this);

            }
        }
        private void configuracion_listView() 
        {
            var lis = lsv_cat;

            lsv_cat.Items.Clear();
            lis.Columns.Clear();
            lis.View = View.Details;
            lis.GridLines = false;
            lis.FullRowSelect = true;
            lis.Scrollable = true;
            lis.HideSelection = false;
            // configurando las columnas
            lis.Columns.Add("ID", 40, HorizontalAlignment.Left);//0
            lis.Columns.Add("Nombre de la categoria", 350, HorizontalAlignment.Left);//1

        }
        private void llenar_listview(DataTable data)
        {
            lsv_cat.Items.Clear();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow dr = data.Rows[i];
                ListViewItem list = new ListViewItem(dr["Id_Cat"].ToString());
                list.SubItems.Add(dr["Categoria"].ToString());
                lsv_cat.Items.Add(list);//para llenar
            }
        }
        private void Cargar_Todas_Categorias()
        {
            RN_Categoria obj = new RN_Categoria();
            DataTable dato = new DataTable();

            dato = obj.RN_mostrar_todas_las_categorias();
            if (dato.Rows.Count > 0)
            {
                llenar_listview(dato);
            }
            else
            {
                lsv_cat.Items.Clear();
            }
        }
        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Marca_Load(object sender, EventArgs e)
        {

        }

        private void gru_det_Click(object sender, EventArgs e)
        {

        }
        private void editar() 
        {

        }
        private void agregar() 
        {

        }

        private void lsv_cat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            pnl_add.Visible = true;
            txt_nom.Focus();
            editarc = false;
        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_cerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_listo_Click(object sender, EventArgs e)
        {
            RN_Categoria obj = new RN_Categoria();
            if (txt_nom.Text.Trim().Length <0)
            {
                MessageBox.Show("Ingrese el nombre de la categoria", "Registrar categoria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;   
            }
            if (editarc== false)
            {
                //agregar nuevo
                obj.RN_Registrar_Categoria(txt_nom.Text);
                pnl_add.Visible = false;
                Cargar_Todas_Categorias();
                txt_nom.Text = "";
            }
            else
            {
                // editar id
                obj.RN_Editar_Categoria(Convert.ToInt32(txt_id.Text), txt_nom.Text);
                pnl_add.Visible = false;
                Cargar_Todas_Categorias();
                txt_nom.Text = "";
                editarc = false;
            }
           
        }
        public bool editarc = false;
        private void bt_edit_Click(object sender, EventArgs e)
        {
            if (lsv_cat.SelectedItems.Count==0)
            {
                MessageBox.Show("Selecciona el Item para editar", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else 
            {
                var lsv = lsv_cat.SelectedItems[0];
                txt_id.Text = lsv.SubItems[0].Text;
                txt_nom.Text = lsv.SubItems[1].Text;

                pnl_add.Visible = true;
                txt_nom.Focus();
                editarc = true;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_Selecc_Click(object sender, EventArgs e)
        {
            if (lsv_cat.SelectedItems.Count == 0)
            {
                MessageBox.Show("Selecciona Una Categoria", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                var lsv = lsv_cat.SelectedItems[0];
                txt_id.Text = lsv.SubItems[0].Text;
                txt_nom.Text = lsv.SubItems[1].Text;

                this.Tag = "A";
                this.Close();
            }
        }

        private void lsv_cat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_Selecc_Click(sender, e);
            }
        }
    }
}
