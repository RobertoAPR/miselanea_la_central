﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Prj_Capa_Datos;
using Prj_Capa_Negocio;



namespace Microsell_Lite.Utilitarios
{
    public partial class Frm_Marca : Form
    {
        public Frm_Marca()
        {
            InitializeComponent();
        }

        private void Frm_Reg_Prod_Load(object sender, EventArgs e)
        {
            configuracion_listView();
            Cargar_Todas_Marcas();
        }

        private void pnl_titu_MouseMove(object sender, MouseEventArgs e)
        {
            Utilitario obj = new Utilitario();

            if (e.Button == MouseButtons.Left)
            {
                obj.Mover_formulario(this);

            }
        }
        private void configuracion_listView() 
        {
            var lis = lsv_mar;

            lsv_mar.Items.Clear();
            lis.Columns.Clear();
            lis.View = View.Details;
            lis.GridLines = false;
            lis.FullRowSelect = true;
            lis.Scrollable = true;
            lis.HideSelection = false;
            // configurando las columnas
            lis.Columns.Add("ID", 40, HorizontalAlignment.Left);//0
            lis.Columns.Add("Nombre de la Marca", 350, HorizontalAlignment.Left);//1

        }
        private void llenar_listview(DataTable data)
        {
            lsv_mar.Items.Clear();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow dr = data.Rows[i];
                ListViewItem list = new ListViewItem(dr["Id_Marca"].ToString());
                list.SubItems.Add(dr["Marca"].ToString());
                lsv_mar.Items.Add(list);//para llenar
            }
        }
        private void Cargar_Todas_Marcas()
        {
            RN_Marca obj = new RN_Marca();
            DataTable dato = new DataTable();

            dato = obj.RN_mostrar_todas_las_Marcas();
            if (dato.Rows.Count > 0)
            {
                llenar_listview(dato);
            }
            else
            {
                lsv_mar.Items.Clear();
            }
        }
        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Marca_Load(object sender, EventArgs e)
        {

        }

        private void gru_det_Click(object sender, EventArgs e)
        {

        }
        private void editar() 
        {

        }
        private void agregar() 
        {

        }

        private void lsv_cat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            pnl_add.Visible = true;
            txt_nom.Focus();
            editarc = false;
        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_cerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_listo_Click(object sender, EventArgs e)
        {
            RN_Marca obj = new RN_Marca();
            if (txt_nom.Text.Trim().Length <0)
            {
                MessageBox.Show("Ingrese el nombre de la Marca", "Registrar categoria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;   
            }
            if (editarc== false)
            {
                //agregar nuevo
                obj.RN_Registrar_Marca(txt_nom.Text);
                pnl_add.Visible = false;
                Cargar_Todas_Marcas();
                txt_nom.Text = "";
            }
            else
            {
                // editar id
                obj.RN_Editar_Marca(Convert.ToInt32(txt_id.Text), txt_nom.Text);
                pnl_add.Visible = false;
                Cargar_Todas_Marcas();
                txt_nom.Text = "";
                editarc = false;
            }
           
        }
        public bool editarc = false;
        private void bt_edit_Click(object sender, EventArgs e)
        {
            if (lsv_mar.SelectedItems.Count==0)
            {
                MessageBox.Show("Selecciona el Item para editar", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else 
            {
                var lsv = lsv_mar.SelectedItems[0];
                txt_id.Text = lsv.SubItems[0].Text;
                txt_nom.Text = lsv.SubItems[1].Text;

                pnl_add.Visible = true;
                txt_nom.Focus();
                editarc = true;
            }
        }

        // pendiente
        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (lsv_mar.SelectedItems.Count == 0)
            {
                MessageBox.Show("Selecciona el Item para Eliminar", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                var lsv = lsv_mar.SelectedItems[0];
                txt_id.Text = lsv.SubItems[0].Text;

                
               Frm_Sino sino = new Frm_Sino();
                sino.ShowDialog();

                if (sino.Tag.ToString()=="Si")
                {
                    RN_Marca obj=new RN_Marca();
                    obj.RN_Eliminar_Marca(Convert.ToInt32(txt_id.Text));
                    Cargar_Todas_Marcas();
                }




            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_Selecc_Click(object sender, EventArgs e)
        {
            if (lsv_mar.SelectedItems.Count == 0)
            {
                MessageBox.Show("Selecciona Una Marca", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                var lsv = lsv_mar.SelectedItems[0];
                txt_id.Text = lsv.SubItems[0].Text;
                txt_nom.Text=lsv.SubItems[1].Text;


                this.Tag = "A";
                this.Close();
            }
        }

        private void lsv_mar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                btn_Selecc_Click(sender,e);
            }
        }
    }
}
