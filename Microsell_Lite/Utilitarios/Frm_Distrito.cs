﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Prj_Capa_Datos;
using Prj_Capa_Negocio;



namespace Microsell_Lite.Utilitarios
{
    public partial class Frm_Distrito : Form
    {
        public Frm_Distrito()
        {
            InitializeComponent();
        }

        private void Frm_Reg_Prod_Load(object sender, EventArgs e)
        {
            configuracion_listView();
            Cargar_Todos_Distrito();
        }

        private void pnl_titu_MouseMove(object sender, MouseEventArgs e)
        {
            Utilitario obj = new Utilitario();

            if (e.Button == MouseButtons.Left)
            {
                obj.Mover_formulario(this);

            }
        }
        private void configuracion_listView() 
        {
            var lis = lsv_dis;

            lsv_dis.Items.Clear();
            lis.Columns.Clear();
            lis.View = View.Details;
            lis.GridLines = false;
            lis.FullRowSelect = true;
            lis.Scrollable = true;
            lis.HideSelection = false;
            // configurando las columnas
            lis.Columns.Add("ID", 40, HorizontalAlignment.Left);//0
            lis.Columns.Add("Nombre de Distrito", 350, HorizontalAlignment.Left);//1

        }
        private void llenar_listview(DataTable data)
        {
            lsv_dis.Items.Clear();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow dr = data.Rows[i];
                ListViewItem list = new ListViewItem(dr["Id_Dis"].ToString());
                list.SubItems.Add(dr["Distrito"].ToString());
                lsv_dis.Items.Add(list);//para llenar
            }
        }
        private void Cargar_Todos_Distrito()
        {
            RN_Distrito obj = new RN_Distrito();
            DataTable dato = new DataTable();

            dato = obj.RN_mostrar_todas_los_Distritos();
            if (dato.Rows.Count > 0)
            {
                llenar_listview(dato);
            }
            else
            {
                lsv_dis.Items.Clear();
            }
        }
        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Marca_Load(object sender, EventArgs e)
        {

        }

        private void gru_det_Click(object sender, EventArgs e)
        {

        }
        private void editar() 
        {

        }
        private void agregar() 
        {

        }

        private void lsv_cat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            pnl_add.Visible = true;
            txt_nom.Focus();
            editarc = false;
        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_cerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_listo_Click(object sender, EventArgs e)
        {
            RN_Distrito obj = new RN_Distrito();
            if (txt_nom.Text.Trim().Length <0)
            {
                MessageBox.Show("Ingrese el nombre de la Marca", "Registrar categoria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;   
            }
            if (editarc== false)
            {
                //agregar nuevo
                obj.RN_Registrar_Distrito(txt_nom.Text);
                pnl_add.Visible = false;
                Cargar_Todos_Distrito();
                txt_nom.Text = "";
            }
            else
            {
                // editar id
                obj.RN_Editar_Distrito(Convert.ToInt32(txt_id.Text), txt_nom.Text);
                pnl_add.Visible = false;
                Cargar_Todos_Distrito();
                txt_nom.Text = "";
                editarc = false;
            }
           
        }
        public bool editarc = false;
        private void bt_edit_Click(object sender, EventArgs e)
        {
            if (lsv_dis.SelectedItems.Count==0)
            {
                MessageBox.Show("Selecciona el Item para editar", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else 
            {
                var lsv = lsv_dis.SelectedItems[0];
                txt_id.Text = lsv.SubItems[0].Text;
                txt_nom.Text = lsv.SubItems[1].Text;

                pnl_add.Visible = true;
                txt_nom.Focus();
                editarc = true;
            }
        }

        // pendiente
          private void btn_delete_Click(object sender, EventArgs e)
          {
            
              if (lsv_dis.SelectedItems.Count == 0)
              {
                  MessageBox.Show("Selecciona el Item para Eliminar", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                  return;
              }
              else
              {
                  var lsv = lsv_dis.SelectedItems[0];
                  txt_id.Text = lsv.SubItems[0].Text;


                 Frm_Sino sino = new Frm_Sino();
                sino.ShowDialog();

                if (sino.Tag.ToString() == "Si")
                {
                    RN_Distrito obj = new RN_Distrito();
                    obj.RN_Eliminar_Distrito(Convert.ToInt32(txt_id.Text));
                    Cargar_Todos_Distrito();
                }



            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
