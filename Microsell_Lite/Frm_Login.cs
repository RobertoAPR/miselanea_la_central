﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraPrintingLinks;
using Prj_Capa_Negocio;


namespace Microsell_Lite
{
    public partial class Frm_Login : Form
    {
        public Frm_Login()
        {
            InitializeComponent();
        }

        private void pml_titu_MouseMove(object sender, MouseEventArgs e)
        {
            Utilitario obj = new Utilitario();

            if (e.Button == MouseButtons.Left)
            {
                obj.Mover_formulario(this);

            }
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuMaterialTextbox2_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void elLabel2_Click(object sender, EventArgs e)
        {

        }

        private void Frm_Login_Load(object sender, EventArgs e)
        {

        }

        private bool Validar_texto()
        {
            Frm_Filtro fil = new Frm_Filtro();

            if (txt_usu.Text.Trim().Length<2)
            {
                fil.Show();
                MessageBox.Show("Ingresa tu Nombre de Usuario", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation
                    );
                txt_usu.Focus();
                return false;
            }
            if (txt_pass.Text.Trim().Length < 2)
            {
                fil.Show();
                MessageBox.Show("Ingrese su Contraseña", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation
                    );
                txt_pass.Focus();
                return false;
            }
            return true;
        }

        private void Hacer_Login()
        {
            RN_Usuario obj = new RN_Usuario();
            DataTable dato = new DataTable();

            string usu = txt_usu.Text;
            string pass = txt_pass.Text;

            int veces = 0;

            if (Validar_texto() == false) return;

            if (obj.RN_Login (usu,pass)==true)
            {
                dato = obj.RN_Buscar_Usuario(usu);
                if (dato.Rows.Count>0)
                {
                    DataRow dr = dato.Rows[0];
                    Cls_Libreria.IdRol = dr["Id_Rol"].ToString();
                    Cls_Libreria.Nombre = dr["Nombres"].ToString();
                    Cls_Libreria.Foto = dr["Ubicacion_Foto"].ToString();
                    Cls_Libreria.Rol = dr["Rol"].ToString();
                    
                }
                this.Hide();
                Frm_Principal pri = new Frm_Principal();
                pri.Show();
            }
            else 
            {
                veces += 1;
                txt_pass.Text = "";
                txt_usu.Text = "";
                txt_usu.Focus();
                MessageBox.Show("El usuario o contraseña son incorrectas, Intentalo nuevamente", "Advertencia de Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (veces==3)
                {
                    MessageBox.Show("Usted ha sobrepasado los limites permitidos de intentos","Advertencia de Login",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    Application.Exit();
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hacer_Login();
        }

        private void txt_usu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txt_pass.Focus();
            }
        }

        private void txt_pass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
    }
}
