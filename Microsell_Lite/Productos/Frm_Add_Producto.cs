﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Prj_Capa_Entidad;
using Prj_Capa_Negocio;
using Microsell_Lite.Utilitarios;
using Microsell_Lite.Proveedor;
using Prj_Capa_Enridad;
using Prj_Capa_Datos;


namespace Microsell_Lite.Productos
{
    public partial class Frm_Add_Producto : Form
    {
        public Frm_Add_Producto()
        {
            InitializeComponent();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool Validar_TextBox()
        {
            Frm_Filtro Fil = new Frm_Filtro();
            Frm_Advertencia Ver = new Frm_Advertencia();//Mensaje cuando ocurre algun eror de validacion
            if (txt_IDProducto.Text.Trim().Length < 2)//Si hay un espacio 
            {
                Fil.Show();
                Ver.Lbl_Msm1.Text = "Ingresa o genera el Id del Producto";
                Ver.ShowDialog();//Muestra el formulario de advertencia 
                
                Fil.Hide();//Oculta
                txt_IDProducto.Focus();
                return false;

            }

            if (txt_DescripcionProd.Text.Trim().Length < 2)
            {
                Fil.Show();//muestra
                Ver.Lbl_Msm1.Text = "Ingresa la Descripcion del Producto";
                Ver.Text = "Ingresa o genera un nombre para el proveedor";
                //Ver.ShowDialog();
                txt_DescripcionProd.Focus();
                Fil.Hide();//oculta
                return false;
            }

            if (txt_ProveedorProd.Text.Trim()=="-")
            {
                Fil.Show();
                Ver.Lbl_Msm1.Text = "Ingresa el ID del Prveedor";
                Ver.ShowDialog();
                txt_ProveedorProd.Focus();
                Fil.Hide();
                return false;
            }//
            if (txt_Marca.Text.Trim()=="-")
            {
                Fil.Show();
                Ver.Lbl_Msm1.Text = "Ingresa el ID de la Marca del Producto";
                Ver.ShowDialog();
                txt_Marca.Focus();
                Fil.Hide();
                return false;
            }//

            if (txt_Categoria.Text.Trim()=="-")
            {
                Fil.Show();
                Ver.Lbl_Msm1.Text = "Ingresa el ID de la Categoria";
                Ver.ShowDialog();
                txt_Categoria.Focus();
                Fil.Hide();
                return false;
            }//

            if (cmb_TipoProducto.SelectedIndex==-1)
            {
                Fil.Show();
                Ver.Lbl_Msm1.Text = "Selecciona un Tipo de Producto";
                Ver.ShowDialog();
                cmb_TipoProducto.Focus();
                Fil.Hide();
                return false;
            }

            if (cmb_UnidadMedida.SelectedIndex == -1)
            {
                Fil.Show();
                Ver.Lbl_Msm1.Text = "Selecciona una Unidad de Medida";
                Ver.ShowDialog();
                cmb_UnidadMedida.Focus();
                Fil.Hide();
                return false;
            }


            //validacion de numeros

            //if (Convert.ToDouble(txt_PrecioUnitario.Text)==0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa el Precio por Unidad");
            //    //Ver.ShowDialog();
            //    txt_PrecioUnitario.Focus();
            //    Fil.Hide();
            //    return false;
            //}

            //if (Convert.ToDouble(txt_Utilidad.Text)== 0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa la utilidad");
            //    //Ver.ShowDialog();
            //    txt_Utilidad.Focus();
            //    Fil.Hide();
            //    return false;
            //}

            //if (Convert.ToDouble(txt_PrecioCompra.Text) == 0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa el Precio de Compra");
            //    //Ver.ShowDialog();
            //    txt_PrecioCompra.Focus();
            //    Fil.Hide();
            //    return false;
            //}

            //if (Convert.ToDouble(txt_FrankUtilidad) == 0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa el Frank del producto");
            //    //Ver.ShowDialog();
            //    txt_FrankUtilidad.Focus();
            //    Fil.Hide();
            //    return false;
            //}

            //if (Convert.ToDouble(txt_PreVentaMenor.Text) == 0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa el Precio de Venta Menor");
            //    //Ver.ShowDialog();
            //    txt_PreVentaMenor.Focus();
            //    Fil.Hide();
            //    return false;
            //}


            //if (Convert.ToDouble(txt_PreVentaMayor.Text) == 0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa el Precio de Venta Mayor");
            //    //Ver.ShowDialog();
            //    txt_PreVentaMayor.Focus();
            //    Fil.Hide();
            //    return false;
            //}

            //if (Convert.ToDouble(txt_Pre_Dolar.Text) == 0)
            //{
            //    Fil.Show();
            //    MessageBox.Show("Ingresa el Precio de Venta en Dolar");
            //    //Ver.ShowDialog();
            //    txt_Pre_Dolar.Focus();
            //    Fil.Hide();
            //    return false;
            //}
            return true;
        }//Este método valida los 3 campos mas importantes de la tabla proveedor que si o si, deben estar colocados, 

        private void btn_BuscarProveedor_Click(object sender, EventArgs e)
        {
            Frm_Filtro fil = new Frm_Filtro();
            Frm_ListadoProveedor lis = new Frm_ListadoProveedor();

            fil.Show();
            lis.ShowDialog();
            fil.Hide();

            if(lis.Tag.ToString() == "A")
            {
                txt_ProveedorProd.Text = lis.lbl_nom.Text;
               lbl_Provee.Text=lis.lbl_id.Text;
                
                
            }
        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_BuscarMarca_Click(object sender, EventArgs e)
        {
            Frm_Filtro fil = new Frm_Filtro();
            Frm_Marca mar = new Frm_Marca();

            fil.Show();
            mar.ShowDialog();
            fil.Hide();

            if (mar.Tag.ToString() == "A")
            {
                txt_Marca.Text = mar.txt_nom.Text;
                lbl_idmarca.Text = mar.txt_id.Text;
            }
        }

        private void btn_BuscarCategoria_Click(object sender, EventArgs e)
        {
            Frm_Filtro fil = new Frm_Filtro();
            Frm_categoria cat = new Frm_categoria();

            fil.Show();
            cat.ShowDialog();
            fil.Hide();

            if (cat.Tag.ToString() == "A")
            {
                txt_Categoria.Text = cat.txt_nom.Text;
                lbl_idcategoria.Text = cat.txt_id.Text;
            }
        }

        string xFotoruta;
        private void picLOG_Click(object sender, EventArgs e)
        {
            var FilePath = string.Empty;

            try
            {
                if (openFileDialog1.ShowDialog()==DialogResult.OK)
                {
                    xFotoruta = openFileDialog1.FileName;
                    picLOG.Load(xFotoruta);
                }
            }
            catch (Exception ex)
            {
                picLOG.Load(Application.StartupPath + @"\user.png");
                xFotoruta = Application.StartupPath + @"\user.png";
                MessageBox.Show("Error al Guardar el personal" + ex.Message);
            }
        }

        private void gru_det_Click(object sender, EventArgs e)
        {

        }

        private void lblFotoPerfil_Click(object sender, EventArgs e)
        {
            picLOG_Click(sender, e);
        }

        private void btn_listo_Click(object sender, EventArgs e)
        {
            if (Validar_TextBox()==true)
            {
                registrar_Producto();
            }
        }

        private void registrar_Producto()
        {
            RN_productos obj = new RN_productos();
            EN_Producto pro= new EN_Producto();

            try
            {
                pro.Idprod = txt_IDProducto.Text;
                pro.DescripcionGeneral = txt_DescripcionProd.Text;
                pro.Idproveedor = lbl_Provee.Text;
                pro.Idmark = Convert.ToInt32(lbl_idmarca.Text);
                pro.Idcat = Convert.ToInt32(lbl_idcategoria.Text);
                pro.TipoProducto = cmb_TipoProducto.Text;
                pro.UndMedida = cmb_UnidadMedida.Text;
                pro.PesoUnit = Convert.ToDouble(txt_PrecioUnitario.Text);
                pro.UtilidadUnit = Convert.ToDouble(txt_Utilidad.Text);
                pro.PreCompra_Sol = Convert.ToDouble(txt_PrecioCompra.Text);
                pro.Frank = Convert.ToDouble(txt_FrankUtilidad.Text);
                pro.Preventa_Dolr = Convert.ToDouble(txt_Pre_Venta_Dolar.Text);
                pro.Preventa_Mnr = Convert.ToDouble(txt_PreVentaMenor.Text);
                pro.Preventa_Myr = Convert.ToDouble(txt_PreVentaMayor.Text);
                pro.PreCompra_Dlr = Convert.ToDouble(txt_Pre_Dolar.Text);
                pro.ValorGeneral = 0;
                pro.Stock = 0;
                if (xFotoruta.Trim().Length < 5)
                {
                    pro.Foto = "-";
                }
                else
                {
                    pro.Foto = xFotoruta;
                }

                obj.RN_Registrar_Producto(pro);
                if (BD_Productos.seguardo==true)
                {
                    if (cmb_TipoProducto.SelectedIndex==0)
                    {
                        //para registrar kardex
                        Registrar_kardex(txt_IDProducto.Text);
                    }
                   

                    Frm_Filtro fil = new Frm_Filtro();

                    fil.Show();
                    MessageBox.Show("El Producto se ha Guardado Exitosamente");
                    fil.Hide();

                    this.Tag="A";
                    this.Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Problemas al guardar" + ex.Message);
            }
        }

        private void Registrar_kardex(string idprod)
        {
            RN_Kardex obj = new RN_Kardex();
            EN_Kardex kr = new EN_Kardex();

            try
            {
                if (obj.RN_Verificar_Producto_siTieneKardex(idprod)==true)
                {
                    return; // ya tiene un kardex
                }
                else
                {
                    string idkardex = RN_TipoDoc.RN_NroID(6);

                    obj.RN_Registrar_Kardex(idkardex, idprod, lbl_Provee.Text);

                    if (BD_Kardex.seguardo == true)
                    {
                        // se actualiza el siguiente num correlativo
                        RN_TipoDoc.RN_Actualizar_SiguienteNro_Correlativo(6);

                        //se trabaja con el detalle del kardex
                        kr.Idkardex = idkardex;
                        kr.Item = 1;
                        kr.Doc_soporte = "000";
                        kr.Det_Operacion = "Inicio de Kardex";

                        //entrada
                        kr.Cantidad_in = 0;
                        kr.Precio_In= 0;
                        kr.Total_In = 0;
                        //salida
                        kr.Cantidad_Out = 0;
                        kr.Precio_out = 0;
                        kr.Total_out= 0;
                        //saldo
                        kr.Cantidad_saldo= 0;
                        kr.Promedio = 0;
                        kr.Total_saldo= 0;

                        obj.RN_Registrar_Detalle_Kardex(kr);

                        if (BD_Kardex.detsaved==true)
                        {

                        }
                 
                    }
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Algo Salio Mal: " + ex.Message, "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txt_PrecioUnitario_TextChanged(object sender, EventArgs e)
        {
            txt_PrecioUnitario.Text = txt_PrecioUnitario.Text.Replace(",", ".");
            txt_PrecioUnitario.SelectionStart = txt_PrecioUnitario.Text.Length;
        }

        private void txt_PrecioUnitario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilitario ui = new Utilitario();
            e.KeyChar=Convert.ToChar(ui.Solo_Numeros(e.KeyChar));
        }

        private void txt_Utilidad_TextChanged(object sender, EventArgs e)
        {

            txt_Utilidad.Text = txt_Utilidad.Text.Replace(",", ".");
            txt_Utilidad.SelectionStart = txt_Utilidad.Text.Length;
        }

        private void txt_Utilidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilitario ui = new Utilitario();
            e.KeyChar = Convert.ToChar(ui.Solo_Numeros(e.KeyChar));
        }

        private void txt_PrecioCompra_TextChanged(object sender, EventArgs e)
        {
            txt_PrecioCompra.Text = txt_PrecioCompra.Text.Replace(",", ".");
            txt_PrecioCompra.SelectionStart = txt_PrecioCompra.Text.Length;
        }

        private void txt_PrecioCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilitario ui = new Utilitario();
            e.KeyChar = Convert.ToChar(ui.Solo_Numeros(e.KeyChar));
        }

        private void txt_FrankUtilidad_TextChanged(object sender, EventArgs e)
        {
            txt_FrankUtilidad.Text = txt_FrankUtilidad.Text.Replace(",", ".");
            txt_FrankUtilidad.SelectionStart = txt_FrankUtilidad.Text.Length;

            try
            {
                if (txt_FrankUtilidad.Text.Trim() == "") return;
                if (txt_PrecioCompra.Text.Trim() == "") return;

                double PrecioCompra = 0;
                double Utilidad = 0;

                PrecioCompra = Convert.ToDouble(txt_PrecioCompra.Text) * Convert.ToDouble(txt_FrankUtilidad.Text);
                txt_PreVentaMenor.Text=PrecioCompra.ToString("###0.00");

                //calcular utilidad
                Utilidad = Convert.ToDouble(txt_PreVentaMenor.Text) - Convert.ToDouble(txt_PrecioCompra.Text);
                txt_Utilidad.Text = Utilidad.ToString("###0.00");
            }
            catch (Exception ex)
            {

              string sms=ex.Message;
            }
        }

        private void txt_FrankUtilidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilitario ui = new Utilitario();
            e.KeyChar = Convert.ToChar(ui.Solo_Numeros(e.KeyChar));
        }

        private void txt_PreVentaMenor_TextChanged(object sender, EventArgs e)
        {
            txt_PreVentaMenor.Text = txt_PreVentaMenor.Text.Replace(",", ".");
            txt_PreVentaMenor.SelectionStart = txt_PreVentaMenor.Text.Length;
        }

        private void txt_PreVentaMenor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilitario ui = new Utilitario();
            e.KeyChar = Convert.ToChar(ui.Solo_Numeros(e.KeyChar));
        }

        private void txt_PreVentaMayor_TextChanged(object sender, EventArgs e)
        {
            txt_PreVentaMayor.Text = txt_PreVentaMayor.Text.Replace(",", ".");
            txt_PreVentaMayor.SelectionStart = txt_PreVentaMayor.Text.Length;
        }

        private void txt_PreVentaMayor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilitario ui = new Utilitario();
            e.KeyChar = Convert.ToChar(ui.Solo_Numeros(e.KeyChar));
        }

        private void txt_Pre_Venta_Dolar_TextChanged(object sender, EventArgs e)
        {
             

            
        }

        private void txt_Pre_Venta_Dolar_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void txt_Pre_Venta_Dolar_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //calcular precio venta
            if (checkBox1.Checked==true)
            {
                txt_Pre_Dolar.Enabled = true;
                txt_Pre_Dolar.Focus();
            }
        }

        private void txt_Pre_VentaDolar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txt_FrankUtilidad.Text.Trim() == "") return;
                if (txt_Pre_Dolar.Text.Trim() == "") return;

                double PrecioCompra = 0;
                double PrecioVentaDolar = 0;
                double Utilidad = 0;


                //hallar precio de compra
                PrecioCompra = Convert.ToDouble(txt_Pre_Dolar.Text) * Convert.ToDouble(lbl_tipoCambio.Text);
                txt_PrecioCompra.Text = PrecioCompra.ToString("###0.00");

                //precio de venta menor
                PrecioCompra = Convert.ToDouble(txt_PrecioCompra.Text) * Convert.ToDouble(txt_FrankUtilidad.Text);
                txt_PreVentaMenor.Text = PrecioCompra.ToString("###0.00");

                //hallar precio venta en dolar
                PrecioVentaDolar = Convert.ToDouble(txt_Pre_Dolar.Text) * Convert.ToDouble(txt_FrankUtilidad.Text);
                txt_Pre_Venta_Dolar.Text = PrecioVentaDolar.ToString("###0.00");

                //calcular utilidad
                Utilidad = Convert.ToDouble(txt_PreVentaMenor.Text) - Convert.ToDouble(txt_PrecioCompra.Text);
                txt_Utilidad.Text = Utilidad.ToString("###0.00");
            }
            catch (Exception ex)
            {

                string sms = ex.Message;
            }
        }
        //en este caso, el campo IDProveedor, el Nombre del Proveedor y su RUC
        //Para ello son las validaciones
    }
}
