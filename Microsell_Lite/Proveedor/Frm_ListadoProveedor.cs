﻿using Prj_Capa_Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Microsell_Lite.Proveedor
{
    public partial class Frm_ListadoProveedor : Form
    {
        public Frm_ListadoProveedor()
        {
            InitializeComponent();
        }
        private void Frm_ListadoProveedor_Load(object sender, EventArgs e)
        {
            Configurar_listView();
            Cargar_Todos_proveedores();
        }
        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Tag = "";
            this.Close();
        }


        private void Configurar_listView()
        {
            var lis = lsv_prove;

            lsv_prove.Items.Clear();
            lis.Columns.Clear();
            lis.View = View.Details;
            lis.GridLines = false;
            lis.FullRowSelect= true;
            lis.Scrollable= true;
            lis.HideSelection = false;
            //configurar columnas
            lis.Columns.Add("ID", 0, HorizontalAlignment.Left);
            lis.Columns.Add("Nombre de Proveedor", 450, HorizontalAlignment.Left);
        }

        private void Llenar_Listview(DataTable data)
        {
            lsv_prove.Items.Clear();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow dr = data.Rows[i];
                ListViewItem list = new ListViewItem(dr["IDPROVEE"].ToString());
                list.SubItems.Add(dr["NOMBRE"].ToString());
                lsv_prove.Items.Add(list);
            }
        }

        private void Cargar_Todos_proveedores()
        {
            RN_Proveedor obj = new RN_Proveedor();
            DataTable dato= new DataTable();

            dato = obj.RN_Mostrar_Todas_Proveedores();
            if (dato.Rows.Count>0)
            {
                Llenar_Listview(dato);
            }
            else
            {
                lsv_prove.Items.Clear();
            }
        }
        
        private void lsv_prove_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lsv_prove.SelectedIndices.Count==0)
            {
                MessageBox.Show("Seleccione un Proveedor", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                lbl_id.Text = lsv_prove.SelectedItems[0].SubItems[0].Text;
                lbl_nom.Text = lsv_prove.SelectedItems[0].SubItems[1].Text;

                this.Tag = "A";
                this.Close();
            }
        }

        private void lsv_prove_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("Seleccione un Proveedor", "Advertencia de Seguridad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                lbl_id.Text = lsv_prove.SelectedItems[0].SubItems[0].Text;
                lbl_nom.Text = lsv_prove.SelectedItems[0].SubItems[1].Text;

                this.Tag = "A";
                this.Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lsv_prove_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
