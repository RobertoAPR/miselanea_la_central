﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsell_Lite.Utilitarios;
using Prj_Capa_Entidad;
using Prj_Capa_Negocio;


namespace Microsell_Lite.Proveedor
{
    public partial class Frm_EditProvee : Form
    {
        public Frm_EditProvee()
        {
            InitializeComponent();
        }

        private void Frm_Reg_Prod_Load(object sender, EventArgs e)
        {
            buscar_Proveedor_paraEditar(this.Tag.ToString());
        }

        private void pnl_titu_MouseMove(object sender, MouseEventArgs e)
        {
            Utilitario obj = new Utilitario();

            if (e.Button == MouseButtons.Left)
            {
                obj.Mover_formulario(this);

            }
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Tag = "";
            this.Close();
        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        string xFotoruta;
        private void lbl_Abrir_Click(object sender, EventArgs e)
        {
            var FilePatch = string.Empty;

            try
            {
                if (openFileDialog1.ShowDialog()==DialogResult.OK)
                {
                    xFotoruta=openFileDialog1.FileName;
                    picLogo.Load(xFotoruta);
                }
            }
            catch (Exception ex)
            {
                picLogo.Load(Application.StartupPath + @"\user.png");
                xFotoruta = Application.StartupPath + @"\user.png";
                MessageBox.Show("Error al guardar el personal" + ex.Message);
                throw;
            }
        }
        private bool validar_textbox()
        {
            Frm_Filtro fill = new Frm_Filtro();
            Frm_Advertencia ver = new Frm_Advertencia();
            if (txt_idprove.Text.Trim().Length < 2) {fill.Show();ver.Lbl_Msm1.Text = "Ingresa o Genera el Id del Proveedor"; ver.ShowDialog();fill.Hide();return false; }
            if (txt_nom.Text.Trim().Length < 2) { fill.Show(); ver.Lbl_Msm1.Text = "Ingresa o Genera el Nombre del Proveedor"; ver.ShowDialog(); fill.Hide(); txt_nom.Focus(); return false; }

            return true;
        }
        private void editar_Proveedor()
        {
            RN_Proveedor obj = new RN_Proveedor();  
            EN_Proveedor pro = new EN_Proveedor();
            try
            {
                pro.Idproveedor=txt_idprove.Text;
                pro.Nombre=txt_nom.Text;
                pro.Direccion=txt_direc.Text;
                pro.Telefono=txt_tel.Text;
                pro.Rubro=txt_rubro.Text;
                pro.Ruc = txt_dni.Text;
                pro.Correo=txt_correo.Text;
                pro.Contacto = txt_contacto.Text;
                pro.Fotologo = xFotoruta;
                obj.RN_Editar_Proveedor(pro);

                LimpiarFrom();
                this.Tag = "A";
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar"+ ex.Message,"From Add Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation) ;
            }
        }
        private void LimpiarFrom()
        {
            txt_tel.Text = "";
            txt_idprove.Text = "";
            txt_nom.Text = "";
            txt_rubro.Text = "";
            txt_correo.Text = "";
            txt_direc.Text = "";
            txt_dni.Text = "";
            txt_contacto.Text = "";
            xFotoruta = "";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btn_listo_Click(object sender, EventArgs e)
        {
            if (validar_textbox()==true)
            {
                editar_Proveedor();
            }
        }

        private void buscar_Proveedor_paraEditar(string idprove)
        {
            RN_Proveedor obj = new RN_Proveedor();
            DataTable data = new DataTable();

            try
            {
                data = obj.RN_Buscar_Todas_Proveedores(idprove);
                if (data.Rows.Count>0)
                {
                    txt_idprove.Text = Convert.ToString(data.Rows[0]["IDPROVEE"]);
                    txt_nom.Text = Convert.ToString(data.Rows[0]["NOMBRE"]);
                    txt_direc.Text = Convert.ToString(data.Rows[0]["DIRECCION"]);
                    txt_correo.Text = Convert.ToString(data.Rows[0]["CORREO"]);
                    txt_tel.Text = Convert.ToString(data.Rows[0]["TELEFONO"]);
                    txt_contacto.Text = Convert.ToString(data.Rows[0]["CONTACTO"]);
                    txt_rubro.Text = Convert.ToString(data.Rows[0]["RUBRO"]);
                    txt_dni.Text = Convert.ToString(data.Rows[0]["RUC"]);
                    xFotoruta = Convert.ToString(data.Rows[0]["FOTO_LOGO"]);

                    picLogo.Load(xFotoruta);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error al guardar" + ex.Message, "From Add Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Tag = "";
            this.Close();
        }
    }
}
