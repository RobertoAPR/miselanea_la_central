﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.ClipboardSource.SpreadsheetML;
using Prj_Capa_Negocio;
using Microsell_Lite.Utilitarios;


namespace Microsell_Lite.Proveedor
{
    public partial class Frm_Explor_Provedoor : Form
    {
        public Frm_Explor_Provedoor()
        {
            InitializeComponent();
        }
       

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Explor_Provedoor_Load(object sender, EventArgs e)
        {
            Configurar_listView();
            Cargar_Todos_Provedores();
        }
        private void pnl_titu_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void pnl_titu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelControl6_Click(object sender, EventArgs e)
        {

        }
        private void Configurar_listView()
        {
            var lis =  lsv_provee;

            lsv_provee.Items.Clear();
            lis.Columns.Clear();
            lis.View = View.Details;
            lis.GridLines = false;
            lis.FullRowSelect = true;
            lis.Scrollable= true;
            lis.HideSelection = false;
            //configuracion de las columnas 
            lis.Columns.Add("ID", 80, HorizontalAlignment.Left);//0
            lis.Columns.Add("Nro DNI",90, HorizontalAlignment.Left);
            lis.Columns.Add("Nombre", 400, HorizontalAlignment.Left);
            lis.Columns.Add("Nro Celular", 90, HorizontalAlignment.Left);
            lis.Columns.Add("Rubro", 180, HorizontalAlignment.Left);
            lis.Columns.Add("Direccion", 286, HorizontalAlignment.Left);
        }
        // llenar lis vew
        private void llenar_lisView(DataTable data)
        {
            lsv_provee.Items.Clear();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow dr =  data.Rows[i];
                ListViewItem list = new ListViewItem(dr["IDPROVEE"].ToString());
                list.SubItems.Add(dr["RUC"].ToString());
                list.SubItems.Add(dr["NOMBRE"].ToString());
                list.SubItems.Add(dr["TELEFONO"].ToString());
                list.SubItems.Add(dr["RUBRO"].ToString());
                list.SubItems.Add(dr["DIRECCION"].ToString());
                lsv_provee.Items.Add(list);
            }
        }
        private void Cargar_Todos_Provedores()
        {
            RN_Proveedor obj = new RN_Proveedor();
            DataTable dato = new DataTable();

            dato = obj.RN_Mostrar_Todas_Proveedores();
            if (dato.Rows.Count>0)
            {
                llenar_lisView(dato);
            }
            else
            {
                lsv_provee.Items.Clear();
            }
        }

        private void Buscar_Provedores(string valor)
        {
            RN_Proveedor obj = new RN_Proveedor();
            DataTable dato = new DataTable();

            dato = obj.RN_Buscar_Todas_Proveedores(valor);
            if (dato.Rows.Count > 0)
            {
                llenar_lisView(dato);
            }
            else
            {
                lsv_provee.Items.Clear();
            }
        }

        private void searchControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (searchControl1.Text.Trim().Length>2)
            {
                Buscar_Provedores(searchControl1.Text);
            }
        }

        private void searchControl1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (searchControl1.Text.Trim().Length > 2)
                {
                    Buscar_Provedores(searchControl1.Text);
                }
                else
                {
                    Cargar_Todos_Provedores();
                }
            }
        }

        private void lsv_provee_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cargar_Todos_Provedores();
        }

        private void bt_copiar_Click(object sender, EventArgs e)
        {
            Frm_Filtro fil = new Frm_Filtro();
            Frm_Advertencia ver = new Frm_Advertencia();

            if (lsv_provee.SelectedIndices.Count==0)
            {
                fil.Show();
                ver.Lbl_Msm1.Text = "Selecciona el item que deseas copiar";
                ver.ShowDialog();
                fil.Hide();
            }
            else
            {
                var lis = lsv_provee.SelectedItems[0];
                string idprovee = lis.SubItems[0].Text;

                Clipboard.SetText(idprovee.Trim());
            }
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            Frm_Filtro fill = new Frm_Filtro();
            Frm_AddProveedor ad = new Frm_AddProveedor();

            fill.Show();
            
            ad.ShowDialog();
            fill.Hide();
            if (ad.Tag.ToString()=="A")
            {
                Cargar_Todos_Provedores();
            }

        }
        
        private void bt_edit_Click(object sender, EventArgs e)
        {
            Frm_Filtro fil = new Frm_Filtro();
            Frm_Advertencia ver = new Frm_Advertencia();
            Frm_EditProvee edi = new Frm_EditProvee();

            
            if (lsv_provee.SelectedIndices.Count==0)
            {
                fil.Show();
                ver.Lbl_Msm1.Text = "Selecciona el Item que desea editar";
           
                ver.ShowDialog();
                fil.Hide();
            }
            else
            {
                var lis = lsv_provee.SelectedItems[0];
                string idprovee=lis.SubItems[0].Text;

                fil.Show();
                edi.Tag= idprovee;
                edi.ShowDialog();
                fil.Hide();

                if (edi.Tag.ToString() == "A")
                {
                    Cargar_Todos_Provedores();
                }
               
            }
        }

        private void btn_cerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnl_titu_MouseMove_1(object sender, MouseEventArgs e)
        {
            Utilitario obj = new Utilitario();

            if (e.Button == MouseButtons.Left)
            {
                obj.Mover_formulario(this);

            }
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void pnl_titu_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void nuevoProvedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt_add_Click(sender, e);
        }

        private void editarProvedorToolStripMenuItem_Click(object sender, EventArgs e)
        {

            bt_edit_Click(sender, e);


        }
    }
}
